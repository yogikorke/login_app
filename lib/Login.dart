import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  String username = "";
  TextEditingController username_controller = new TextEditingController();
  TextEditingController password_controller = new TextEditingController();

  password_submitted() {
    debugPrint(username);

    setState(() {
      username = username_controller.text;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.blue[900],
          title: Center(child: Text("Login"))),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.asset("images/logo.png", width: 100, height: 100),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: username_controller,
              decoration: InputDecoration(
                  labelText: "Username",
                  hintText: "Username",
                  icon: Icon(
                    Icons.account_box,
                    color: Colors.blue[900],
                    size: 50,
                  )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: password_controller,
              obscureText: true,
              decoration: InputDecoration(
                  labelText: "Password",
                  hintText: "Password",
                  icon: Icon(
                    Icons.lock,
                    color: Colors.blue[900],
                    size: 50,
                  )),
            ),
          ),
          Center(
            child: Wrap(children: <Widget>[
                 RaisedButton(
                  onPressed: () => password_submitted(),
                  color: Colors.blue[900],
                  child: Text(
                    "Login",
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  )),
            ]),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
                child: Text(
              "Hello $username",
              style: TextStyle(fontSize: 20),
            )),
          )
        ],
      ),
    );
  }
}
